---
title: "Das ganze Spiel"
author: "Team Foo"
date: "5 7 2022"
output: 
  html_document:
    keep_md: true
    toc: true
    toc_float: true
    number_sections: true
---


# Thema 1

Wir wollen etwas analysieren:

# Test, wenn nur Knit gedrückt wird.



* a
* b  
* c





<!-- ToDo: Wollen wir nicht lieber xyz? -->

Dabei nutzen wir die Methode **OLS**.

## Verwendeter Datensatz
Wir verwenden hier die Daten vom Paket gfc.

```r
summary(gfc)
```

```
##    quarter               GDPE             GDPU      
##  Length:60          Min.   : 86.70   Min.   : 80.4  
##  Class :character   1st Qu.: 93.95   1st Qu.: 93.2  
##  Mode  :character   Median :102.95   Median :103.0  
##                     Mean   :101.86   Mean   :102.6  
##                     3rd Qu.:109.47   3rd Qu.:114.9  
##                     Max.   :115.70   Max.   :119.5
```
## Analyse
Zur Analyse definieren eine neue Variable:

```r
gfc$gdpeegdpuu = gfc$GDPE/gfc$GDPU
```

Diese Variable ist interessant, weil:  

1. Item 1  
2. Item 2
3. Item 3
    + Item 3a
    + Item 3b
    

## Plot

```r
plot(gfc$gdpeegdpuu)
```

![](demo_final_files/figure-html/unnamed-chunk-3-1.png)<!-- -->


## Interpretation

Aus der Analyse können wir ableiten, dass der Trend nach unten geht. Dies zeigt uns auch der Plot. Die ökonomischen Implikationen sind:

